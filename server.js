import http from 'http';
import WebSocket from 'ws';

const server = http.createServer();
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws) => {
  ws.on('message', (message) => { ws.send(`echo ${message}`); });
  ws.send('hello');
});

wss.on('connection', (ws) => {
  ws.send('hello');
});

wss.on('connection', (ws) => {
  ws.send('hello');
});

function sendMessageFromServer() {    
  wss.on('connection', (ws) => {
    ws.send('hello');
  });
}

setInterval(sendMessageFromServer, 1000);

export default server;